## TWOH Content Blocks
Extension containing a list of basic TYPO3 Content Elements based on Content Blocks.

### Minimum requirements
* **PHP** 8
* **composer** ^2
* **TYPO3** 12

### Setup
* install Extension via Composer or FTP
* include Extension in TypoScript **ROOT Template**