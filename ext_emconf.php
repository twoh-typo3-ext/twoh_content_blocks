<?php

$EM_CONF['twoh_content_blocks'] = array(
    'title' => 'TWOH TYPO3 Content Blocks',
    'description' => 'Extension containing a list of basic TYPO3 Content Elements based on Content Blocks.',
    'category' => 'plugin',
    'author' => 'Andreas Reichel',
    'author_email' => 'a.reichel91@outlook.com',
    'author_company' => 'TWOH digital',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '1.0.4',
    'constraints' => array(
        'depends' => array(
            'typo3' => '12.4.99',
            'php' => '8.0-8.3',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
